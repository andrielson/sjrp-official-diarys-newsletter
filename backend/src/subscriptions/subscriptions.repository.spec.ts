import { Test, TestingModule } from '@nestjs/testing';
import { SubscriptionsRepository } from './subscriptions.repository';

describe('SubscriptionsRepository', () => {
  let repository: SubscriptionsRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubscriptionsRepository],
    }).compile();

    repository = module.get<SubscriptionsRepository>(SubscriptionsRepository);
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });
});
