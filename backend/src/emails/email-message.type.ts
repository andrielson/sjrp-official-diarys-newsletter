import { SendMailOptions } from 'nodemailer';

export type EmailMessage = Pick<
  SendMailOptions,
  'html' | 'subject' | 'text' | 'to'
>;
