import { EmailMessage } from './email-message.type';

export class UserAlreadySubscribedEmail implements EmailMessage {
  readonly html = [
    '<h3>Olá,</h3>',
    '<p>Seu e-mail já está cadastrado no Boletim do Diário Oficial de São José do Rio Preto-SP!</p>',
    '<p>Atenciosamente,</p>',
    '<h6>Oleg</h6>',
  ].join('');
  readonly subject = 'Você já está inscrito';
  readonly text = [
    'Olá,\n',
    'Seu e-mail já está cadastrado no Boletim do Diário Oficial de São José do Rio Preto-SP!',
    'Atenciosamente,',
    'Oleg',
  ].join('\n');
  constructor(readonly to: string) {}
}
