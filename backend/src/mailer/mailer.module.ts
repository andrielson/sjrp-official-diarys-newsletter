import { Module } from '@nestjs/common';
import { MailerProvider } from './mailer.provider';
import { MailerService } from './mailer.service';

@Module({
  exports: [MailerService],
  providers: [MailerProvider, MailerService],
})
export class MailerModule {}
