import {
  IsArray,
  IsBoolean,
  IsDefined,
  IsEmail,
  IsNotEmpty,
} from 'class-validator';

export class CreateSubscriptionDto {
  @IsDefined()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsArray()
  @IsDefined()
  @IsNotEmpty()
  keywords: string[];

  @IsBoolean()
  @IsDefined()
  @IsNotEmpty()
  consent: boolean;
}
