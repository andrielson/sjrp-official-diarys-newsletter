import { Module } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionsController } from './subscriptions.controller';
import { SubscriptionsRepository } from './subscriptions.repository';
import { MailerModule } from 'src/mailer/mailer.module';

@Module({
  controllers: [SubscriptionsController],
  imports: [MailerModule],
  providers: [SubscriptionsRepository, SubscriptionsService],
})
export class SubscriptionsModule {}
