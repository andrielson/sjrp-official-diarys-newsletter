export class BusinessLogicException {
  constructor(public readonly code: string, public readonly message: string) {}
}
