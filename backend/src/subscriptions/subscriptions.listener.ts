import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { SignUpConfirmationEmail } from 'src/emails/signup-confirmation/signup-confirmation.email';
import { UserAlreadySubscribedEmail } from 'src/emails/user-already-subscribed.email';
import { MailerService } from 'src/mailer/mailer.service';

@Injectable()
export class SubscriptionsListener {
  constructor(private readonly mailerService: MailerService) {}

  @OnEvent('subscription.existing', { async: true })
  sendUserAlreadyExistsEmail(email: string) {
    return this.mailerService.sendEmail(new UserAlreadySubscribedEmail(email));
  }

  @OnEvent('subscription.new', { async: true })
  sendSubscriptionConfirmationEmail(email: string) {
    return this.mailerService.sendEmail(
      new SignUpConfirmationEmail(email, 'eita nois'),
    );
  }
}
