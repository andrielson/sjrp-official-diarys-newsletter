export class Subscription {
  email: string;
  keywords: string[];
  createdAt: Date;
  verified: boolean;
}
