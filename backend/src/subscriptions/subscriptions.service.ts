import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { BusinessLogicErrors } from 'src/shared/enums/business-logic-errors.enum';
import { BusinessLogicException } from 'src/shared/errors/business-logic.exception';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { UpdateSubscriptionDto } from './dto/update-subscription.dto';
import { SubscriptionEvent } from './events/subscription.event';
import { SubscriptionsRepository } from './subscriptions.repository';

@Injectable()
export class SubscriptionsService {
  constructor(
    private readonly repository: SubscriptionsRepository,
    private readonly eventEmitter: EventEmitter2,
  ) {}

  async create(dto: CreateSubscriptionDto) {
    const { email, keywords, consent } = dto;

    if (!consent) {
      throw new BusinessLogicException(
        BusinessLogicErrors.E0001,
        'Usuário não aceitou os termos de uso!',
      );
    }

    const count = await this.repository.countByEmail(email);
    if (count > 0) {
      return this.eventEmitter.emitAsync(
        'subscription.existing',
        new SubscriptionEvent(email),
      );
    }

    const { acknowledged } = await this.repository.insertOne({
      email,
      keywords,
      createdAt: new Date(),
      verified: false,
    });

    if (!acknowledged) {
      throw new BusinessLogicException(
        BusinessLogicErrors.E0002,
        'Falha ao cadastrar o e-mail!',
      );
    }
    return this.eventEmitter.emitAsync(
      'subscription.new',
      new SubscriptionEvent(email),
    );
  }

  findAll() {
    return `This action returns all subscriptions`;
  }

  findOne(id: number) {
    return `This action returns a #${id} subscription`;
  }

  update(id: number, dto: UpdateSubscriptionDto) {
    return `This action updates a #${id} subscription`;
  }

  remove(id: number) {
    return `This action removes a #${id} subscription`;
  }
}
