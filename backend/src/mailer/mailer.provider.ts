import { FactoryProvider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createTransport, Transporter } from 'nodemailer';
import { MAILER } from 'src/constants';

export const MailerProvider: FactoryProvider<Transporter> = {
  inject: [ConfigService],
  provide: MAILER,
  useFactory: (config: ConfigService) =>
    createTransport({
      service: config.get<string>('MAILER_SERVICE'),
      auth: {
        user: config.get<string>('MAILER_USER'),
        pass: config.get<string>('MAILER_PASSWORD'),
      },
    }),
};
