import { readFileSync } from 'fs';
import { resolve } from 'path';
import { EmailMessage } from '../email-message.type';

export class SignUpConfirmationEmail implements EmailMessage {
  private readonly bufferEncoding: BufferEncoding = 'utf-8';
  private readonly fileOptions = { encoding: this.bufferEncoding };
  private readonly htmlfile = resolve(__dirname, 'signup-confirmation.xhtml');
  private readonly textfile = resolve(__dirname, 'signup-confirmation.txt');
  readonly html: string;
  readonly subject = 'Confirme sua inscrição';
  readonly text: string;

  constructor(readonly to: string, confirmationLink: string) {
    this.html = this.getContentFromFile(this.htmlfile, confirmationLink);
    this.text = this.getContentFromFile(this.textfile, confirmationLink);
  }

  private getContentFromFile(file: string, confirmationLink: string) {
    const content = readFileSync(file, this.fileOptions);
    return content.replace('LINK AQUI', confirmationLink);
  }
}
