import { Collection, Db } from 'mongodb';

export abstract class AbstractRepository<T> {
  protected collection: Collection<T>;
  constructor(collectionName: string, connection: Db) {
    this.collection = connection.collection<T>(collectionName);
  }
}
