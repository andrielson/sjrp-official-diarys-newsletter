import { FactoryProvider } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Db, MongoClient } from 'mongodb';
import { DATABASE_CONNECTION } from 'src/constants';

export const DatabaseProvider: FactoryProvider<Promise<Db>> = {
  inject: [ConfigService],
  provide: DATABASE_CONNECTION,
  useFactory: (config: ConfigService): Promise<Db> =>
    MongoClient.connect(config.get<string>('MONGODB_URI')).then((client) =>
      client.db(config.get<string>('MONGODB_DB')),
    ),
};
