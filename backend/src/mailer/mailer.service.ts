import { Inject, Injectable } from '@nestjs/common';
import { Transporter, SendMailOptions } from 'nodemailer';
import { MAILER } from 'src/constants';
import { EmailMessage } from 'src/emails/email-message.type';

@Injectable()
export class MailerService {
  private readonly defaultOptions: SendMailOptions = {
    from: {
      name: 'Boletim Diário Oficial - Rio Preto',
      address: 'andrielson@carroporassinatura.blog.br',
    },
  };

  constructor(@Inject(MAILER) private readonly transporter: Transporter) {}

  sendEmail(message: EmailMessage) {
    return this.transporter.sendMail({ ...this.defaultOptions, ...message });
  }
}
