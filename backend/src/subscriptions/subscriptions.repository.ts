import { Inject } from '@nestjs/common';
import { Db } from 'mongodb';
import { DATABASE_CONNECTION } from 'src/constants';
import { AbstractRepository } from 'src/database/abstract-repository';
import { Subscription } from './entities/subscription.entity';

export class SubscriptionsRepository extends AbstractRepository<Subscription> {
  constructor(@Inject(DATABASE_CONNECTION) connection: Db) {
    super('subscriptions', connection);
  }
  countByEmail = async (email: string) => {
    return this.collection.countDocuments({ email });
  };
  insertOne = async (subscription: Subscription) => {
    const { acknowledged, insertedId } = await this.collection.insertOne(
      subscription,
    );
    return { acknowledged, insertedId };
  };
}
