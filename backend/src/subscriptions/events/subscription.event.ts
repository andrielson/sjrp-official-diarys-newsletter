export class SubscriptionEvent {
  constructor(public readonly email: string) {}
}
